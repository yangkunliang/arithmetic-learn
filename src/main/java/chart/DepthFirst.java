package chart;

import java.util.*;

/**
 * 深度优先
 *
 * @Author: ykl
 * @Description: Created by ykl on 2019/7/26.
 */
public class DepthFirst {

    private static void depthFirst() {
        Stack<Map<String, Object>> nodeStack = new Stack<Map<String, Object>>();
        Map<String, Object> node = new HashMap<String, Object>();
        nodeStack.add(node);
        while (!nodeStack.isEmpty()) {
            node = nodeStack.pop();
            System.out.println(node);
            List<Map<String, Object>> children = getChildren(node);
            if (children != null && !children.isEmpty()) {
                for (Map child : children) {
                    nodeStack.push(child);
                }
            }
        }
    }

    private static List<Map<String, Object>> getChildren(Map<String, Object> node) {
        List<Map<String, Object>> list = new
                ArrayList<Map<String, Object>>();

        return list;
    }
}
