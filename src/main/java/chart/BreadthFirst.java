package chart;

import java.util.*;

/**
 * 广度优先
 *
 * @Author: ykl
 * @Description: Created by ykl on 2019/7/26.
 */
public class BreadthFirst {
    private void breadthFirst() {
        Deque<Map<String, Object>> nodeDeque = new ArrayDeque<Map<String, Object>>();
        Map<String, Object> node = new HashMap<String, Object>();
        nodeDeque.add(node);
        while (!nodeDeque.isEmpty()) {
            node = nodeDeque.peekFirst();
            System.out.println(node);
            //获得节点的子节点，对于二叉树就是获得节点的左子结点和右子节点
            List<Map<String, Object>> children = getChildren(node);
            if (children != null && !children.isEmpty()) {
                for (Map child : children) {
                    nodeDeque.add(child);
                }
            }
        }
    }

    private List<Map<String, Object>> getChildren(Map<String, Object> node) {
        List<Map<String, Object>> list = new
                ArrayList<Map<String, Object>>();

        return list;
    }
}
